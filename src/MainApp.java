import java.util.Arrays;

public class MainApp {

    public static void main(String[] args) {
        UserDatabase userDatabase = new UserDatabase();

        User user1 = new User("omer", "omer@mail.com", "antwerp, belgium");
        User user2 = new User("john", "john@mail.com", "antwerp, belgium");
        User user3 = new User("jack", "jaxck@mail.com", "antwerp, belgium");


        userDatabase.addUser(user1);
        userDatabase.addUser(user2);
        userDatabase.addUser(user3);

        System.out.println(Arrays.toString(userDatabase.getUsers()));
    }


}
