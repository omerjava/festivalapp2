import java.util.Arrays;

public class UserDatabase {

    private User[] users = { };

    public User[] getUsers() {
        return users;
    }

    public void setUsers(User[] users) {
        this.users = users;
    }
    public void addUser(User newUser) {
        this.users = Arrays.copyOf(users, users.length+1);
        users[users.length-1] = newUser;
    }
}
